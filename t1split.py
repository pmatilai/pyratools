#!/usr/bin/python3

# Split a MIDI Type-1 song file into individual Type-0 track files
# the way Pyramid expects them.

import mido
import sys
import os.path

if __name__ == '__main__':
    mid1 = mido.MidiFile(sys.argv[1])
    if mid1.type == 1:
        projpath = 'PYRA_%s' % sys.argv[2]
        os.mkdir(projpath)
        print(len(mid1.tracks))
        for i in range(1, len(mid1.tracks)):
            trkname = os.path.join(projpath, 'track%02d.mid' % i)
            origname = mid1.tracks[i].name
            mid0 = mido.MidiFile(type=0)
            mid0.tracks.append(mid1.tracks[i])
            print('Saving track %02d (%s) as %s' % (i,origname, trkname))
            mid0.save(filename=trkname)
