#!/usr/bin/python3

# Merge MIDI Type-0 tracks into Type-1 MIDI files the way Pyramid expects
# them for patterns.

import mido
import sys
import os.path

if __name__ == '__main__':
    m1 = mido.MidiFile(type=1)
    for arg in sys.argv[1:]:
        m0 = mido.MidiFile(arg)
        if m0.type != 0:
            continue
        
        m1.tracks.append(m0.tracks[0])

    m1.save('merged.mid')
