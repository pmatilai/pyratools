#!/usr/bin/python3

# Dump all events in a MIDI file

import mido
import sys

if __name__ == '__main__':
    total = 0
    for arg in sys.argv[1:]:
        print('%s:' % (arg))
        cur = 0
        mf = mido.MidiFile(arg)
        mf.print_tracks()
