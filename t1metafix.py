#!/usr/bin/python3

# Type-1 MIDI files commonly a meta-only track as the first track,
# containing all the metadata common to all tracks on the song,
# such as tempo and time signature. Pyramid (ab)uses the Type-1 format
# for storing patterns, and the presence of the meta-track causes
# pattern numbers to be off by one to what one expects. Work around
# this by copying the relevant common info to all tracks, and stripping
# the meta-only track.

import mido
import sys

if __name__ == '__main__':
    for arg in sys.argv[1:]:
        mf = mido.MidiFile(arg)
        if mf.type != 1:
            continue

        meta = True
        t0 = mf.tracks[0]
        for e in t0:
            if not e.is_meta:
                meta = False
                break
        
        if meta:
            print('stripping meta track')
            # Copy common meta (time signature etc) to each track
            for t in mf.tracks[1:]:
                for e in t0:
                    if e.type == 'track_name':
                        continue
                    t.insert(0, e)

            mf.tracks.remove(mf.tracks[0])
            mf.save(arg)
