# pyratools

Miscellaneous utility scripts related to the Squarp Pyramid sequencer.
Python 3.x with MIDO library required, the latter is generally installable
with `pip install mido`.

## t1merge

Merge multiple Type-0 MIDI files into one Type-1 file as used for patterns
by the Pyramid.

Eg. `t1merge.py *.mid` to merge all Type-0 `.mid` files in the current
directory into one Type-1, saved as `merged.mid`.

## t1split

Split a Type-1 MIDI song file into layout expected by the Pyramid.

Eg. `t1split.py LightMyFire.mid`

## t1metafix

When importing Type-1 MIDI files exported from popular DAWs, patterns
numbers appear off by one. This utility can be used to fixup that.

Eg. `t1metafix.py track05.mid`. Note that this will overwrite the original
file, so make sure you have a backup!

## trkdump

A generic utility for inspecting MIDI file contents.

Eg. `trkdump.py track01.mid`
